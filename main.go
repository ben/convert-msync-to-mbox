package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"regexp"
	"strconv"
	"strings"
	"time"
)

func main() {
	splitMessages(os.Stdin)
}

func splitMessages(r io.Reader) {
	var buf []byte
	s := bufio.NewScanner(r)

	for {
		buf = buf[:0]
		var haveMessage bool
		for s.Scan() {
			line := s.Text()
			buf = append(buf, line...)
			buf = append(buf, '\n')
			if line == "--------------" {
				haveMessage = true
				break
			}
		}

		if err := s.Err(); err != nil {
			panic(err)
		}

		if !haveMessage {
			if len(buf) != 0 {
				panic("extra at end: " + string(buf))
			}
			break
		}

		msg := parseOneMessage(string(buf))
		printMessage(msg)
	}
}

const (
	idPattern   = `(\d+)`
	tstamp      = `(\d{4,}-\d\d-\d\dT\d\d:\d\d:\d\d\.\d\d\dZ)`
	displayName = `([^\n]*) \(@([^@\(\)]+(?:@[^@\(\)]+)?)\)( \[bot\])?`
)

var (
	notificationPattern   = regexp.MustCompile(`\Anotification id: ` + idPattern + `\nat ` + tstamp + `, ` + displayName + `( favorited your post:| boosted your post:| mentioned you:|'s poll ended:| followed you\.)\n`)
	statusPattern         = regexp.MustCompile(`\Astatus id: ` + idPattern + `\nurl: ([^\n]+)\nauthor: ` + displayName + `\n`)
	replyToPattern        = regexp.MustCompile(`\A(?:boosted by: ` + displayName + `\n)?(?:reply to: ` + idPattern + `\n)?(?:boost of: (.*)\n)?`)
	bodyPattern           = regexp.MustCompile(`\A(?:cw: (.*)\n)?(?:body: ([\s\S]*?)\n)?((?:attached: .*\n(?:description: [\s\S]*?\n)?)+)?visibility: (public|unlisted|private|direct)\nposted on: ` + tstamp + `\n(\d+) favs \| (\d+) boosts \| (\d+) replies\n--------------\n\z`)
	mentionAtStartPattern = regexp.MustCompile(`\A@([^@ ]+(?:@[^@ ]+)?) `)
)

type userData struct {
	DispName string
	Addr     string
	IsBot    bool
}

type messageData struct {
	Notification struct {
		ID     string
		Time   time.Time
		User   userData
		Action string
	}

	StatusID string
	URL      string
	Author   userData

	BoostedBy userData
	ReplyToID string
	BoostOfID string

	ContentWarning string
	Body           string
	Visibility     string
	PostedOn       time.Time
	Favs           int
	Boosts         int
	Replies        int
}

func parseOneMessage(data string) messageData {
	var message messageData

	if strings.HasPrefix(data, "notification id: ") {
		match := notificationPattern.FindStringSubmatch(data)
		if match == nil {
			panic("couldn't parse notification: " + data)
		}
		data = data[len(match[0]):]
		message.Notification.ID = match[1]
		ts, err := time.Parse(time.RFC3339Nano, match[2])
		if err != nil {
			panic(err)
		}
		message.Notification.Time = ts
		message.Notification.User.DispName = match[3]
		message.Notification.User.Addr = match[4]
		message.Notification.User.IsBot = match[5] != ""
		message.Notification.Action = match[6]

		if match[6] == " followed you." && data == "--------------\n" {
			return message
		}
	}

	if !strings.HasPrefix(data, "status id: ") {
		panic("non status?: " + data)
	}

	{
		match := statusPattern.FindStringSubmatch(data)
		if match == nil {
			panic("couldn't parse status: " + data)
		}
		data = data[len(match[0]):]
		message.StatusID = match[1]
		message.URL = match[2]
		message.Author.DispName = match[3]
		message.Author.Addr = match[4]
		message.Author.IsBot = match[5] != ""
	}

	if strings.HasPrefix(data, "reply to: ") || strings.HasPrefix(data, "boosted by: ") {
		match := replyToPattern.FindStringSubmatch(data)
		if match == nil || len(match[0]) == 0 {
			panic("couldn't parse reply: " + data)
		}
		data = data[len(match[0]):]
		message.BoostedBy.DispName = match[1]
		message.BoostedBy.Addr = match[2]
		message.BoostedBy.IsBot = match[3] != ""
		message.ReplyToID = match[4]
		message.BoostOfID = match[5]
	}

	{
		match := bodyPattern.FindStringSubmatch(data)
		if match == nil {
			panic("couldn't parse body: " + data)
		}
		data = data[len(match[0]):]
		message.ContentWarning = match[1]
		message.Body = match[2]
		// TODO: match[3] (attachments)
		message.Visibility = match[4]
		ts, err := time.Parse(time.RFC3339Nano, match[5])
		if err != nil {
			panic(err)
		}
		message.PostedOn = ts
		message.Favs, err = strconv.Atoi(match[6])
		if err != nil {
			panic(err)
		}
		message.Boosts, err = strconv.Atoi(match[7])
		if err != nil {
			panic(err)
		}
		message.Replies, err = strconv.Atoi(match[8])
		if err != nil {
			panic(err)
		}
	}

	return message
}

func (u userData) String() string {
	return strconv.Quote(u.DispName) + ` <` + u.Addr + `>`
}

func printMessage(msg messageData) {
	if msg.Notification.Action != "" || msg.BoostOfID != "" || (msg.Visibility != "public" && msg.Visibility != "unlisted") {
		// TODO
		return
	}

	ts := msg.PostedOn
	author := msg.Author
	cw := msg.ContentWarning
	body := msg.Body
	msgid := msg.StatusID
	replyTo := msg.ReplyToID

	fmt.Println("From MSYNC", ts.Format(time.ANSIC))
	fmt.Println("Date:", ts.Format(time.ANSIC))
	fmt.Println("From:", author)
	field := "To:"
	for {
		match := mentionAtStartPattern.FindString(body)
		if match == "" {
			break
		}
		body = body[len(match):]
		fmt.Println(field, match[:len(match)-1])

		field = "Cc:"
	}
	if cw != "" {
		fmt.Println("Subject:", cw)
	} else {
		fmt.Println("Subject:", strings.Replace(body, "\n", "\n  ", -1))
	}
	fmt.Println("Content-Type: text/plain; charset=utf-8")
	fmt.Println("Content-Length:", len(body))
	fmt.Println("Message-ID: <" + msgid + "@msync>")
	if replyTo != "" {
		fmt.Println("In-Reply-To: <" + replyTo + "@msync>")
	}
	fmt.Println()
	fmt.Println(body)
	fmt.Println()
}
